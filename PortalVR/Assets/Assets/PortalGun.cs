﻿using UnityEngine;
using UnityEngine.Events;
using Valve.VR;
using Valve.VR.InteractionSystem;
public class PortalGun : MonoBehaviour
{
    public Portal Red;
    public Portal Blue;

    public SteamVR_Action_Boolean fireAction;

    private Interactable interactable;


    void Start()
    {
        interactable = GetComponent<Interactable>();
    }
    void Update()
    {
        if (interactable.attachedToHand != null)
        {
            SteamVR_Input_Sources source = interactable.attachedToHand.handType;
            if (fireAction[source].stateDown)
            {
                Fire();
            }
        }
    }
    public void Fire()
    {
        
        
            Ray ray = new Ray(transform.position, transform.right);

            Debug.DrawRay(transform.position, transform.forward, Color.red);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject.layer != 10) return;
                
                if (Input.GetMouseButtonDown(0))
                {
                    Red.transform.rotation = Quaternion.LookRotation(hit.normal);
                    Red.transform.position = hit.point + Red.transform.forward * 0.6f;
                }
                else
                {
                    Blue.transform.rotation = Quaternion.LookRotation(hit.normal);
                    Blue.transform.position = hit.point + Blue.transform.forward * 0.6f;
                }
            }
        
    }
}
