using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrollingTurret : MonoBehaviour
{
    public float rotationSpeed = 10f; // rotation speed of the turret
    public float rayDistance = 10f; // distance of the ray
    public LayerMask layerMask; // layer mask to detect objects
    public AudioSource audioSource; // audio source to play the sound
    public AudioClip hitSound; // sound to play when an object is detected

    private Transform objectInRay; // object in the ray

    void Start()
    {
        StartCoroutine(Patrol());
    }

    IEnumerator Patrol()
    {
        while (true)
        {
            if (objectInRay == null)
            {
                // rotate the turret to patrol
                transform.Rotate(0, rotationSpeed * Time.deltaTime, 0);
            }
            else
            {
                // rotate the turret towards the object
                Vector3 direction = objectInRay.position - transform.position;
                Quaternion lookRotation = Quaternion.LookRotation(direction);
                transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation, rotationSpeed * Time.deltaTime);

                // play the sound
                if (!audioSource.isPlaying)
                {
                    audioSource.clip = hitSound;
                    audioSource.Play();
                }
            }

            yield return null;

            // shoot a ray
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.forward, out hit, rayDistance, layerMask))
            {
                objectInRay = hit.transform;
            }
            else
            {
                objectInRay = null;
            }
        }
    }
}