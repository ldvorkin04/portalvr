using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ball : MonoBehaviour
{
    public float speed = 10f; // movement speed of the sphere
    public float destroyTime = 10f; // time until the sphere is destroyed after colliding with an object

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.velocity = transform.forward * speed; // move the sphere forward
    }

    void OnCollisionEnter(Collision collision)
    {
        // if the sphere collides with an object, destroy it after the specified time
        Destroy(gameObject, destroyTime);
    }
}