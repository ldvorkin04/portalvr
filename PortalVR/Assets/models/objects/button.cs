using UnityEngine;

public class DoorController : MonoBehaviour
{
    public Animator buttonAnimator;
    public Animator doorAnimator;
    private bool buttonPressed;
    private string boolName = "Press";
    private string booldoorName = "Open";

    private void Start()
    {
        buttonAnimator = GameObject.FindWithTag("Button").GetComponent<Animator>();
        doorAnimator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (buttonPressed && Input.GetKeyUp(KeyCode.Space))
        {
            buttonAnimator.SetBool(boolName, false);
            doorAnimator.SetBool(booldoorName, true);
            buttonPressed = false;
        }
        else if (!buttonPressed && Input.GetKeyDown(KeyCode.Space))
        {
            buttonAnimator.SetBool(boolName, true);
            doorAnimator.SetBool(boolName, false);
            buttonPressed = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Cube") || other.gameObject.CompareTag("Interactable"))
        {
            buttonAnimator.SetBool(boolName, true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Cube") || other.gameObject.CompareTag("Interactable"))
        {
            buttonAnimator.SetBool(boolName, false);
        }
    }
}